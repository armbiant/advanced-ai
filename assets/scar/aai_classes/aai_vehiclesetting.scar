
local toRad = math.pi/180

---@class AAI_C_VehicleSetting
---@field retreatDanger number
---@field minRetreatDanger number
---@field ignoreFacing boolean
---@field baseCloseInDistance number
---@field maxCloseInDistance number
---@field shouldntRush boolean
---@field extraAttackMove boolean
---@field closeInThreshold number
---@field closeInDistanceScaleWithSafety number
---@field rushBehindThreshold number
---@field reverseThreshold number
---@field steps integer
---@field retreatTimer number
---@field retreatTimerMissingHealthExtraDuration number
---@field lowHealthThreshold number
---@field facingTimer number
---@field facingLockTimer number
---@field facingAngleThreshold number
---@field dangerRequiredToRetreatIncreaseNearHQ number
---@field enemyAntiTankValueMult number
AAI_C_VehicleSetting = {
	ignoreFacing = false,
	baseCloseInDistance = 10.0,
	maxCloseInDistance = 35.0,
	shouldntRush = false,
	extraAttackMove = true,
	retreatDanger = 25.0,
	minRetreatDanger = 5.0,
	enemyAntiTankValueMult = 1.25,
	closeInThreshold = 7.0,
	closeInDistanceScaleWithSafety = 2.0,
	rushBehindThreshold = 150.0,
	reverseThreshold = 100 * toRad,
	steps = 5,
	retreatTimer = 5.5,
	retreatTimerMissingHealthExtraDuration = 6.5,
	lowHealthThreshold = 0.575,
	facingTimer = 30.0,
	facingLockTimer = 4.0,
	facingAngleThreshold = 30 * toRad,
	dangerRequiredToRetreatIncreaseNearHQ = 65.0,
}

---@param retreatDanger number
---@param minRetreatDanger number
---@param ignoreFacing boolean
---@param baseCloseInDistance number
---@param maxCloseInDistance number
---@param shouldntRush boolean
---@param extraAttackMove boolean
---@param enemyAntiTankValueMult? number
---@param o? AAI_C_VehicleSetting
---@return AAI_C_VehicleSetting
function AAI_C_VehicleSetting:newPreset(retreatDanger, minRetreatDanger, ignoreFacing, baseCloseInDistance, maxCloseInDistance, shouldntRush, extraAttackMove, enemyAntiTankValueMult, o)
	o = o or {}

	o.ignoreFacing = ignoreFacing
	o.baseCloseInDistance = baseCloseInDistance
	o.maxCloseInDistance = maxCloseInDistance
	o.shouldntRush = shouldntRush
	o.extraAttackMove = extraAttackMove
	o.retreatDanger = retreatDanger
	o.minRetreatDanger = minRetreatDanger
	o.enemyAntiTankValueMult = enemyAntiTankValueMult or 1.25

	setmetatable(o, {__index = self})
	return o
end

---@param o? AAI_C_VehicleSetting
---@return AAI_C_VehicleSetting
function AAI_C_VehicleSetting:new(o)
	o = o or {}
	setmetatable(o, {__index = self})
	return o
end
